import time
import serial
import paho.mqtt.client as paho

#mqtt parameters
broker="192.168.#.###" #Enter here your mqtt server ip adress
port=1883

if __name__ == '__main__':
    #Establish Mqtt server connection
    client= paho.Client("WeatherOS")
    client.connect(broker,port)
    #Establish connection with the serial communication with arduino.
    #You may need to replace "/dev/ttyUSB0" with "COM5" for example depending
    #on your SO and port in use.
    ser = serial.Serial('/dev/ttyUSB0', 9600, timeout=1)
    ser.flush()
    ct = 0
    while True:
        if ser.in_waiting > 0: #Wait for the arduino print
            if ct == 0: #First reading will be "LoRa Begin", so we can´t split it
                Received = ser.readline()
                print("Starting...")
                ct = 1
            else: #After the first reading we will receive the station data
                Received = ser.readline().decode("utf-8")
                #Splits the received string, creating a list
                ReceivedList = Received.split(',')
                #We'll use different vars for each element of the list
                Wind = ReceivedList[0]
                MaxWind = ReceivedList[1]
                Rain = ReceivedList[2]
                Temp = ReceivedList[3]
                MaxTemp = ReceivedList[4]
                MinTemp = ReceivedList[5]
                Humidity = ReceivedList[6]
                UvIndex = ReceivedList[7]
                Pressure = ReceivedList[8]
                Direction = ReceivedList[9]
                #Sends data to the mqtt server
                #You may want to change the mqtt topics
                client.publish("weather/Temp", Temp)
                client.publish("weather/Humidity", Humidity)
                time.sleep(300) #Waits 5 minutes
