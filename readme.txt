WeatherOS V.4.0

In this version the receiver code has been changed, but the weather station itself has not received any updates.
The purpose of this release is to demonstrate how it is possible to use the station's data in a smarthome environment.
The LoRa module is connected to an arduino which communicates via USB (serial) with the raspberry, 
which in turn sends the data to a mqtt server. This data can then easily be used to create automations such as turning on heaters below a certain temperature. 
The mqtt server can be hosted on the raspberry itself.
In this version the data is also transmitted by bluetooth through an HM-10 module (Optimal compatibility with all kinds of smartphones).
In the example "weatheros.py" only the temperature and humidity data are sent to the mqtt server, but it can be changed as needed.
The method of checking data integrity in the receiver has been improved in this version.
Although the data is checked to avoid crashes, the file "keep.py" can be executed to restart the software in case of a crash.
For use with mysql database and http server the version 3.1 is very stable, 
but I will update it soon (better graphical interface with weather forecast based on station data and improvements in the receiver code).


