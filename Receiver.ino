#include <SPI.h> //LoRa Module uses SPI
#include <LoRa.h> //LoRa Library
#include <SoftwareSerial.h> //Defines the pins for the Bluetooth module
SoftwareSerial bluetooth(6, 7); //Bluetooth RX/TX pins
//Strings for LoRa data
String LoRa_Wind;
String LoRa_MaxWind;
String LoRa_Direction;
String LoRa_Rain;
String LoRa_Temp;
String LoRa_MaxTemp;
String LoRa_MinTemp;
String LoRa_Humidity;
String LoRa_UvIndex;
String LoRa_Pressure;

String Direction; //Wind direction data after integrity check
int Dir_Length = 0; //Used to check direction data integrity

//Received data will be converted to float to
//avoid sending corrupted data that may cause 
//a crash on the raspberry software
//(that may be caused by long distance or interferences)
float Wind;
float MaxWind;
float Rain;
float Temp;
float MaxTemp;
float MinTemp;
float Humidity;
float UvIndex;
float Pressure;

void setup() {
  Serial.begin(9600); //For RaspberryPi
  bluetooth.begin(9600); //For Bluetooth
  
  //Starts LoRa
  while (!Serial);
  Serial.println("LoRa Receiver");
  if (!LoRa.begin(433E6)) {
    Serial.println("Starting LoRa failed!");
    while (1);
    }
}

void loop() {
  int packetSize = LoRa.parsePacket();
  if (packetSize) {
    // received a packet
    // read packet
    while (LoRa.available()) {
      //Receives and splits the data
      LoRa_Wind = LoRa.readStringUntil(';');
      LoRa_MaxWind = LoRa.readStringUntil(';');
      LoRa_Direction = LoRa.readStringUntil(';');
      LoRa_Rain = LoRa.readStringUntil(';');
      LoRa_Temp = LoRa.readStringUntil(';');
      LoRa_MaxTemp = LoRa.readStringUntil(';');
      LoRa_MinTemp = LoRa.readStringUntil(';');
      LoRa_Humidity = LoRa.readStringUntil(';');
      LoRa_UvIndex = LoRa.readStringUntil(';');
      LoRa_Pressure = LoRa.readStringUntil(';');

      direction_check(); //Checks for corrupted data

      Wind = LoRa_Wind.toFloat();
      MaxWind = LoRa_MaxWind.toFloat();
      Rain = LoRa_Rain.toFloat() / 0.2794;
      Temp = LoRa_Temp.toFloat();
      MaxTemp = LoRa_MaxTemp.toFloat();
      MinTemp = LoRa_MinTemp.toFloat();
      Humidity = LoRa_Humidity.toFloat();
      UvIndex = LoRa_UvIndex.toFloat();
      Pressure = LoRa_Pressure.toFloat();
      
      //Sends data splited by "," to the raspberry
      Serial.print(Wind);
      Serial.print(",");
      Serial.print(MaxWind);
      Serial.print(",");
      Serial.print(((float)Rain * 0.2794), 4);
      Serial.print(",");
      Serial.print(Temp);
      Serial.print(",");
      Serial.print(MaxTemp);
      Serial.print(",");
      Serial.print(MinTemp);
      Serial.print(",");
      Serial.print(Humidity);
      Serial.print(",");
      Serial.print(UvIndex);
      Serial.print(",");
      Serial.print(Pressure);
      Serial.print(",");
      Serial.print(Direction);

      //Sends BLE module data
      bluetooth.println("-----------");
      bluetooth.print("Temp: ");
      bluetooth.print(Temp);
      bluetooth.println("C");
      bluetooth.print("Max: ");
      bluetooth.print(MaxTemp);
      bluetooth.print("C");
      bluetooth.print(" Min: ");
      bluetooth.print(MinTemp);
      bluetooth.println("C");
      bluetooth.print("Humidade: ");
      bluetooth.print(Humidity);
      bluetooth.println("%");
      bluetooth.print("Vento: ");
      bluetooth.print(Wind);
      bluetooth.print("Km/h ");
      bluetooth.println(Direction);
      bluetooth.print("Max: ");
      bluetooth.print(MaxWind);
      bluetooth.println("Km/h");
      bluetooth.print("P: ");
      bluetooth.print(Pressure);
      bluetooth.println("hPa");
      bluetooth.print("Chuva: ");
      bluetooth.print(((float)Rain * 0.2794), 4);
      bluetooth.println("mm");
      bluetooth.print("UV: ");
      bluetooth.println(UvIndex);
      bluetooth.println("-----------");
    }
  }
}

//Checks if Wind Direction data contains the expected
//number of characters to avoid corrupted data
void direction_check(){
  Dir_Length = LoRa_Direction.length();
  if((Dir_Length == 1) || (Dir_Length == 2)){
    Direction = LoRa_Direction;
  } else {
    Direction = "Error";
  }

}
